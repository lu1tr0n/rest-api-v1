FROM adoptopenjdk/openjdk11:jdk-11.0.3_7
VOLUME /tmp
ADD target/rest-api-v1-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT exec java -jar /app.jar
