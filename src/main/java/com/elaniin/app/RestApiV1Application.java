package com.elaniin.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiV1Application {

	public static void main(String[] args) {
		SpringApplication.run(RestApiV1Application.class, args);
	}

}
