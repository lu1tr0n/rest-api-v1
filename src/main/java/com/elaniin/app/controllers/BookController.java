package com.elaniin.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elaniin.app.models.Book;

@RestController
@RequestMapping(value = "/api/v1")
public class BookController {
	
	private List<Book> listado;
	
	public BookController() {
		listado = new ArrayList<Book>();
		listado.add(new Book("Harry Potter: Reliquias de la muerte", "J. J. Rowling"));
		listado.add(new Book("La metamorfosis", "Franz Kafka"));
	}
	
	@RequestMapping(value = "/books")
	public Map<String, List<Book>> index() {
		Map<String, List<Book>> result = new HashMap<String, List<Book>>();
		result.put("Libros", listado);
		return result;
	}
	
}
