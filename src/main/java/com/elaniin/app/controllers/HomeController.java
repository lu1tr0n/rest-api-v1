package com.elaniin.app.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class HomeController {
	
	@RequestMapping(method = RequestMethod.GET)
	public Map<String, String> index() {
		Map<String, String> result = new HashMap<String, String>();
		result.put("Version", "1");
		result.put("api-name", "rest-api-v1");
		return result;
	}

}
