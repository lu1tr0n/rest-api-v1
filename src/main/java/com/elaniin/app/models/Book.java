package com.elaniin.app.models;

import java.io.Serializable;

public class Book implements Serializable{

	private static final long serialVersionUID = -3886176257405267097L;
	private String nombreLibro;
	private String nombreAutor;
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Book(String nombreLibro, String nombreAutor) {
		super();
		this.nombreLibro = nombreLibro;
		this.nombreAutor = nombreAutor;
	}

	public String getNombreLibro() {
		return nombreLibro;
	}
	
	public void setNombreLibro(String nombreLibro) {
		this.nombreLibro = nombreLibro;
	}
	
	public String getNombreAutor() {
		return nombreAutor;
	}
	
	public void setNombreAutor(String nombreAutor) {
		this.nombreAutor = nombreAutor;
	}
}
